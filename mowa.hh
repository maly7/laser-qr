#include "speak_lib.h"
#include <iostream>
#include <string>

int SynthCallback(short *wav, int numsamples, espeak_EVENT *events);
void espeak_init(void);
void espeak_speak(std::string tekst);
