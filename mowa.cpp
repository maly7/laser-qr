#include "mowa.hh"
#include <iostream>
#include <string>

int SynthCallback(short *wav, int numsamples, espeak_EVENT *events)
{
    return 0;
}

void espeak_init(void)
{
  espeak_Initialize(AUDIO_OUTPUT_PLAYBACK, 0, NULL, 0 );
  espeak_SetVoiceByName("pl");
}

void espeak_speak(std::string tekst)
{
  unsigned int flags=espeakCHARS_AUTO | espeakENDPAUSE;
  espeak_Synth( tekst.c_str(), tekst.size()+1, 0,POS_CHARACTER,0, flags, NULL, NULL );
  //espeak_Synchronize( );
}
