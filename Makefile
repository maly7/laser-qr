SRC = main.cpp mowa.cpp

__start__: program
	./a.out

program: ${SRC}
	g++ `pkg-config --cflags --libs opencv` ${SRC}  -lzbar -lespeak -lopencv_core -lopencv_imgproc -lopencv_highgui -lopencv_objdetect -lopencv_imgcodecs -lopencv_videoio
