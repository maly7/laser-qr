#include <iostream>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <string>
#include <vector>
#include <zbar.h>
#include "speak_lib.h"

#include "mowa.hh"

using namespace std;
using namespace cv;
using namespace zbar;

int main(void)
{
	char key = 0;
	VideoCapture capture = VideoCapture(0); // otwieranie kamery

	if (!capture.isOpened()) // jesli nie udalo sie otworzyc kamery
	{
		cout << "Nie mozna otworzyc kamery" << endl;
		return -1;
	}

	espeak_init();

	ImageScanner scanner;
	scanner.set_config(ZBAR_NONE, ZBAR_CFG_ENABLE, 1);
	double dWidth = capture.get(CV_CAP_PROP_FRAME_WIDTH);
	double dHeight = capture.get(CV_CAP_PROP_FRAME_HEIGHT);

	namedWindow("Obraz", CV_WINDOW_AUTOSIZE);
	Mat img, gray, hsv, laser;

	namedWindow("Control", CV_WINDOW_AUTOSIZE);
	int hmin = 146, hmax = 179;
	int smin = 6, smax = 255;
	int vmin = 216, vmax = 255;
	cvCreateTrackbar("LowH", "Control", &hmin, 179); //Hue (0 - 179)
	cvCreateTrackbar("HighH", "Control", &hmax, 179);
	cvCreateTrackbar("LowS", "Control", &smin, 255); //Saturation (0 - 255)
	cvCreateTrackbar("HighS", "Control", &smax, 255);
	cvCreateTrackbar("LowV", "Control", &vmin, 255); //Value (0 - 255)
	cvCreateTrackbar("HighV", "Control", &vmax, 255);

	while(key != 27) // jesli nie wcisnieto ESC
	{
		capture >> img; // przechwycenie obrazu z kamery do img

		cvtColor(img, gray, CV_BGR2GRAY); // konwersja obrazu na szary
		cvtColor(img, hsv, CV_BGR2HSV); // konwersja obrazu na hsv


		/* Wykrywanie lasera */
    inRange(hsv, Scalar(hmin, smin, vmin), Scalar(hmax, smax, vmax), laser);
    GaussianBlur(laser, laser, Size(5, 5), 2.2);

		imshow("Control", laser);

		Point maxPos;
		minMaxLoc(laser, NULL, NULL, NULL, &maxPos);
		circle(img, maxPos, 20, Scalar(0,0,255),3);
		/* Wykrywanie lasera */

		/* Wykrywanie kodow Qr */
		Image image(gray.cols, gray.rows, "Y800", (uchar *) gray.data, gray.cols * gray.rows);
		int n = scanner.scan(image);
		for(Image::SymbolIterator symbol = image.symbol_begin(); symbol != image.symbol_end(); ++symbol)
		{
			vector<Point> vp;
			int n = symbol->get_location_size();
			for(int i=0;i<n;i++)
				vp.push_back(Point(symbol->get_location_x(i),symbol->get_location_y(i)));

			Point2f pts[4];
			RotatedRect r = minAreaRect(vp);
			r.points(pts);
			for(int i=0;i<4;i++)
				line(img,pts[i],pts[(i+1)%4],Scalar(255,0,0),3);

			//cout << symbol->get_data() << endl;
		}
		/* Wykrywanie kodow Qr */

		imshow("Obraz", img); // wyswietlenie obrazu z kamery
		key = waitKey(5);
	}
	capture.release(); // zwolnienie kamery
	return 0;
}
